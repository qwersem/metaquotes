#include "CLogReader.h"
#include <cstdio>
#include <cstring>

CLogReader::CLogReader(const char *name, const char *filter) noexcept
    : file(nullptr)
{
    if (!name || !name[0])
    {
        printf("File name is empty.\n");
        return;
    }

    if(!Open(name))
    {
        perror("File open error.");
        return;
    }

    SetFilter(filter);
}

CLogReader::~CLogReader()
{
    Close();
}

bool CLogReader::Open(const char *path)
{
    file = fopen(path, "r");
    if (file == NULL)
        return false;
    return true;
}

void CLogReader::Close()
{
    if (file)
    {
        if (fclose(file) == EOF)
            perror("File close error.");
        else
            file = NULL;
    }
    else
        printf("The File Descriptor is empty.\n");
}

bool CLogReader::SetFilter(const char *filter)
{
    if (filter == nullptr || strlen(filter) == 0)
        return false;

    memset(&this->filter, 0, sizeof(this->filter));
    auto it_ = (char*)this->filter;
    auto it = (char*)filter;

    if (it[0] == '?')
    {
        printf("Invalid mask value. Enter a mask without a char \'?\' in start. It's unsupported.\n");
        return false;
    }

    while (*it)
    {
        if (!((*it == '*' || *it == '?') && (*(it+1) == '*' || *(it+1) == '?')))
            *it_++ = *it;
        ++it;
    }

    for (int i = 0; i < strlen(this->filter) - 1; ++i) {
        if (this->filter[i] == '?')
        {
            printf("Invalid mask value. After a char \'?\' must be nothing.\n");
            memset(&this->filter, 0, sizeof(this->filter));
            return false;
        }
    }
    return true;
}

[[maybe_unused]] bool CLogReader::GetFilter(char *buf, const size_t size)
{
    size_t len = strlen(filter);
    if (size > len)
    {
        strncpy(buf, filter, len);
        return true;
    }
    return false;
}

#if defined(_WIN32) || defined(_WIN64)
/**
 * Custom realisation of POSIX method getline(...) for getting last text line from opened file.
 * It's for suppording Windows version.
*/
ssize_t getline(char **lineptr, size_t *n, FILE *stream)
{
    size_t pos;
    int c;

    if (lineptr == NULL || stream == NULL || n == NULL)
    {
        errno = EINVAL;
        return -1;
    }

    c = getc(stream);
    if (c == EOF)
        return -1;

    if (*lineptr == NULL)
    {
        *lineptr = (char*)malloc(128);
        if (*lineptr == NULL) {
            return -1;
        }
        *n = 128;
    }

    pos = 0;
    while(c != EOF)
    {
        if (pos + 1 >= *n)
        {
            size_t new_size = *n + (*n >> 2);
            if (new_size < 128)
                new_size = 128;
            char *new_ptr = (char*)realloc(*lineptr, new_size);
            if (new_ptr == NULL)
                return -1;
            *n = new_size;
            *lineptr = new_ptr;
        }

        ((unsigned char *)(*lineptr))[pos ++] = c;
        if (c == '\n')
            break;
        c = getc(stream);
    }

    (*lineptr)[pos] = '\0';
    return pos;
}
#endif

bool CLogReader::GetNextLine(char *buf, const size_t size)
{
    char * line = nullptr;
    size_t len = 0;
    ssize_t read;

    if (!file)
        return false;

    if ((read = getline(&line, &len, file)) == -1)
        return false;

    FilterState state = Unknown;
    auto it = (char*)this->filter;
    int maskSize(0);
    size_t pos(0);

    char* last = &line[strlen(line) - 1];
    if (*last == '\n' || *last == '\r')
        *last = '\0';

    if (*it == '*')
    {
        ++it;
        state = Postfix;
    }

    while (*it)
    {
        if (*it == '*')
        {
            state = (FilterState)(state | Prefix);
            size_t tmp = Match(line, it - maskSize, maskSize);
            if (state == Suffix)
            {
                if (tmp <= pos)
                {
                    pos = 0;
                    break;
                }
            }
            else if (tmp != maskSize) // Prefix
                break;

            if (*(it+1) != '\0')
                state = Postfix;

            pos = tmp;
            maskSize = -1;
        }
        else if (*it == '?')
        {
            state = (FilterState)(state | Prefix);
            pos = Match(line, it - maskSize, maskSize);

            if (pos != strlen(line) - 1)
            {
                pos = 0;
                break;
            }
        }
        ++maskSize;
        ++it;
    }

    if (*it == '\0')
    {
        if (state == Unknown)
        {
            if (pos == 0)
                pos = Match(line, it - maskSize, maskSize);

            if (maskSize != strlen(line))
                pos = 0;
        }
        else if (state == Postfix)
        {
            pos = Match(line, it - maskSize, maskSize);
            if (pos != strlen(line))
                pos = 0;
        }
    }
    state = Unknown;

    if (pos > 0)
    {
        len = strlen(line);
        if (size > len)
        {
            strncpy(buf, line, len);
            buf[len] = '\0';
            return true;
        }
        perror("Not enough buffer size to write current string.\n");
        return false;
    }
    memset(buf, 0, size);
    return true;
}

size_t CLogReader::Match(const char *text, const char *image, size_t len)
{
    size_t j = 0;
    int *pi = (int*)malloc(strlen(image) * sizeof(int));
    PrefixFunction(image, pi, strlen(image));
    for (size_t i = 0; i < strlen(text); ++i)
    {
        while ((j > 0) && (text[i] != image[j]))
            j = pi[j - 1];
        if (text[i] == image[j])
            ++j;
        if (j == len)
        {
            free(pi);
            return i + 1;
        }
    }
    free(pi);
    return 0;
}

void CLogReader::PrefixFunction(const char *s, int *pi, size_t size)
{
    pi[0] = 0;
    for (size_t i = 1; i < size; ++i)
    {
        int j = pi[i - 1];
        while ((j > 0) && (s[i] != s[j]))
            j = pi[j - 1];
        if (s[i] == s[j])
            ++j;
        pi[i] = j;
    }
}
