#pragma once

#include <iostream>

class CLogReader
{
    /**
     * Enum of filter states
    */
    enum FilterState : uint8_t
    {
        Unknown,
        /// abc*
        Prefix,
        /// *abc
        Postfix,
        /// *abc*
        Suffix
    };

    /// File descriptor
    FILE* file;
    /// Filter storage buffer
    char filter[UINT8_MAX*8];

public:

    CLogReader(const char* name = "", const char* filter = "") noexcept;

    CLogReader(const CLogReader& that) = delete;

    ~CLogReader();

    /**
     * File opening method
     * \param[in] path The path to the file
     * \return Execution state
     */
    bool Open(const char* path);

    /**
     * File closing method
    */
    void Close();

    /**
     * Filter setting method
     * \param[in] filter String that consists of words and characters '*' and '?' for a regular expression.
     * \return Execution state
     *
     * Examples:
     * abc*, *abc, abc*cde, abc*cde*
     * abc?, *abc?, *abc*cde?
     * *abc*, *abc*cde, *abc*cde*
    */
    bool SetFilter(const char* filter);

    /**
     * Current filter getting method
     * \param[out] buf Filter storage buffer for writing
     * \param[in] size Size of buffer
     * \return Execution state
    */
    [[maybe_unused]]
    bool GetFilter(char* buf, size_t size);

    /**
     * Method for matching a regular expression in a string
     * \param[out] buf Buffer to write a string
     * \param[in] size Size of buffer
     * \return Execution state
     *
     * If a buffer is empty after success execution then the string doesn't match the regular expression.
    */
    bool GetNextLine(char* buf, size_t size);

private:

    /**
     * Prefix-function method
     * \param[out] s Pointer on string
     * \param[in] pi Pointer on array to write a prefix-function
     * \param[in] size Size of string s and array pi
     *
     * More here: https://cp-algorithms.com/string/prefix-function.html.
    */
    void PrefixFunction(const char* s, int* pi, size_t size);

    /**
     * String matching method based on the prefix-function
     * \param[in] text Pointer on text
     * \param[in] pi Pointer on current mask for matching with the text
     * \param[in] len Mask length
     * \return Current mask position in the text
    */
    size_t Match(const char* text, const char* image, size_t len);

};
