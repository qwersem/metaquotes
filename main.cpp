#include "CLogReader.h"
#include <cstring>

int main(int argc, char *argv[])
{
    const size_t size = 2048;
    char buf[size];
    memset(buf, 0, size);

    if (argc == 3)
    {
        auto reader = new CLogReader(argv[1], argv[2]);
        while (reader->GetNextLine(buf, size))
        {
            if (buf[0] != '\0')
                printf("%s\n", buf);
        }
        delete reader;
    }
    else
        printf("Need to enter arguments to run the program\n"
#if defined(linux)
               "Example: ./metaquotes"
#elif defined(_WIN32) || defined(_WIN64)
               "Example: metaquotes.exe"
#endif
               " <Filepath> <filter>\n");
    return EXIT_SUCCESS;
}
