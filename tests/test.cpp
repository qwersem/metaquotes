#include "../CLogReader.h"
#include <gtest/gtest.h>
#include <map>

static const size_t BUF_SIZE = 2048;

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

void TestFunction(char *buf, const char* fileName, const char* filter, uint countResult)
{
    char * line = nullptr;
    size_t len = 0;
    FILE* fp = fopen(fileName, "r");
    uint count = 0;

    auto reader = std::make_unique<CLogReader>(fileName, filter);
    while (reader->GetNextLine(buf, BUF_SIZE))
    {
        getline(&line, &len, fp);
        char* last = &line[strlen(line) - 1];
        if (*last == '\n' || *last == '\r')
            *last = '\0';

        if (buf[0] != '\0')
        {
            ++count;
            ASSERT_STREQ(line, buf);
        }
    }
    fclose(fp);
    ASSERT_EQ(count, countResult);
}

TEST(CLogReaderTest, PreTest)
{
    char buf[BUF_SIZE];
    memset(buf, 0, BUF_SIZE);
    const char * fileName = "../../presample.txt";
    FILE* fp = fopen(fileName, "r");
    uint count = 0;

    std::vector<std::string> vec
    {
            "a", "aa",
            "ab", "aaa",
            "aba", "abb",
            "aaa", "aba",
            "abb"
    };

    auto reader = std::make_unique<CLogReader>(fileName, "a*");
    while (reader->GetNextLine(buf, BUF_SIZE))
    {
        if (buf[0] != '\0')
        {
            ASSERT_STREQ(vec[count].c_str(), buf);
            ++count;
        }
    }
    fclose(fp);
    ASSERT_EQ(count, 9);
}

/// abc, abc*, abc*cde, abc*cde*
/// *abc, *abc*
/// abc?, *cde?
TEST(CLogReaderTest, SmallTest)
{
    char buf[BUF_SIZE];
    memset(buf, 0, BUF_SIZE);
    const char * fileName = "../../sample.txt";

    std::vector<std::pair<uint, std::string>> params
            {
                    { 1, "Utilitatis causa amicitia est quaesita." },
                    { 1, "Utilitatis cau*" },
                    { 1, "*quaesita." },
                    { 0, "Utilitatis cau*aesita.\n" },
                    { 1, "Utilitatis cau*aesita." },
                    { 1, "Lorem*eti*" },
                    { 0, "eti*Lorem*" },
                    { 1, "Lorem?" },
                    { 0, "*tamen?" },
                    { 1, "*maximum malum neglegi?" },
                    { 2, "*le*" },
                    { 0, "*tamen?" },
                    { 0, "*tamen?" },
            };

    for (auto const element : params)
    {
        TestFunction(buf, fileName, element.second.c_str(), element.first);
    }
}

/// abc*, abc*cde, abc*cde*
/// *abc, *abc*
/// abc?, *abc?
TEST(CLogReaderTest, UsualTest)
{
    char buf[BUF_SIZE];
    memset(buf, 0, BUF_SIZE);
    const char * fileName = "../../sample-2mb-text-file.txt";

    std::vector<std::pair<uint, std::string>> params
            {
                    { 33, "Diam*" },
                    { 6, "*consectetur adipiscing." },
                    { 1, "Diam*sodales." },
                    { 3, "Diam quam*massa*" },
                    { 1, "*sodales?" },
                    { 2088, "*vitae*" }
            };

    for (auto const element : params)
    {
        TestFunction(buf, fileName, element.second.c_str(), element.first);
    }
}

/// *abc*, abc*cde*
/// abc*cde*fgh*ijk*, *abc*cde*fgh*ijk*
TEST(CLogReaderTest, LoadTest)
{
    char buf[BUF_SIZE];
    memset(buf, 0, BUF_SIZE);
    const char * fileName = "../../sample-20mb-text-file.txt";

    std::vector<std::pair<uint, std::string>> params
            {
                    { 20880, "*vitae*" },
                    { 110, "Lorem*eti*" },
                    { 10, "Lorem*eti*malesuada*ullamcorper*" },
                    { 0, "Lorem*eti*malesuada*ullamcorper*Ornare*" },
                    { 10, "*eti*malesuada*ullamcorper*Ornare*" }
            };

    for (auto const element : params)
    {
        TestFunction(buf, fileName, element.second.c_str(), element.first);
    }
}

TEST(CLogReaderTest, OpenUnknownFile)
{
    const size_t bufSize = 2048;
    char buf[bufSize];
    memset(buf, 0, bufSize);
    auto reader = std::make_unique<CLogReader>("unknown.txt");
    while (reader->GetNextLine(buf, bufSize));
    reader.release();

    reader = std::make_unique<CLogReader>();
    while (reader->GetNextLine(buf, bufSize));
    reader.release();

    const char* name = nullptr;
    reader = std::make_unique<CLogReader>(name);
    while (reader->GetNextLine(buf, bufSize));
}

TEST(CLogReaderTest, SetFilter)
{
    auto reader = std::make_unique<CLogReader>("../../sample.txt", "ab");
    char filter[BUF_SIZE];
    reader->GetFilter(filter, BUF_SIZE);
    ASSERT_STREQ(filter, "ab");

    reader->SetFilter("*ab*");
    memset(filter, 0, BUF_SIZE);
    reader->GetFilter(filter, BUF_SIZE);
    ASSERT_STREQ(filter, "*ab*");

    reader->SetFilter("ab*cd");
    memset(filter, 0, BUF_SIZE);
    reader->GetFilter(filter, BUF_SIZE);
    ASSERT_STREQ(filter, "ab*cd");

    reader->SetFilter("ab**cd");
    memset(filter, 0, BUF_SIZE);
    reader->GetFilter(filter, BUF_SIZE);
    ASSERT_STREQ(filter, "ab*cd");

    reader->SetFilter("*ab*cd*");
    memset(filter, 0, BUF_SIZE);
    reader->GetFilter(filter, BUF_SIZE);
    ASSERT_STREQ(filter, "*ab*cd*");

    reader->SetFilter("****ab*cd*****");
    memset(filter, 0, BUF_SIZE);
    reader->GetFilter(filter, BUF_SIZE);
    ASSERT_STREQ(filter, "*ab*cd*");

    reader->SetFilter("ab?");
    memset(filter, 0, BUF_SIZE);
    reader->GetFilter(filter, BUF_SIZE);
    ASSERT_STREQ(filter, "ab?");

    reader->SetFilter("ab?cd");
    memset(filter, 0, BUF_SIZE);
    reader->GetFilter(filter, BUF_SIZE);
    ASSERT_STREQ(filter, "");

    reader->SetFilter("ab???????????????????");
    memset(filter, 0, BUF_SIZE);
    reader->GetFilter(filter, BUF_SIZE);
    ASSERT_STREQ(filter, "ab?");

    reader->SetFilter("?ab?");
    memset(filter, 0, BUF_SIZE);
    reader->GetFilter(filter, BUF_SIZE);
    ASSERT_STREQ(filter, "");

    reader->SetFilter("ab???????????????????cd");
    memset(filter, 0, BUF_SIZE);
    reader->GetFilter(filter, BUF_SIZE);
    ASSERT_STREQ(filter, "");

    reader->SetFilter("ab*?");
    memset(filter, 0, BUF_SIZE);
    reader->GetFilter(filter, BUF_SIZE);
    ASSERT_STREQ(filter, "ab?");

    reader->SetFilter("ab?*");
    memset(filter, 0, BUF_SIZE);
    reader->GetFilter(filter, BUF_SIZE);
    ASSERT_STREQ(filter, "ab*");
}
